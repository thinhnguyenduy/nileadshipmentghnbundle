<?php

namespace Nilead\ShipmentsGHNBundle\DependencyInjection;

use Nilead\ResourceBundle\DependencyInjection\AbstractResourceExtension;


/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class NileadShipmentGHNExtension extends AbstractResourceExtension
{
    protected $configFiles = array(
        'services',
    );
}
