<?php
/**
 * Created by Rubikin Team.
 * Date: 4/20/14
 * Time: 2:00 AM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentsGHNBundle\Tests\Message;

use Nilead\ShipmentBundle\Tests\TestCase;
use Nilead\ShipmentBundle\Tests\TraitPackageTest;
use Nilead\ShipmentsGHNBundle\Message\CancelOrderRequest;

class CancelOrderRequestTest extends TestCase
{
    protected $request;

    use TraitPackageTest;

    public function setUp()
    {
        $this->request = new CancelOrderRequest();
        $this->request->setServices($this->getHttpClient(), $this->getHttpRequest());
    }

    public function testGetData()
    {
        $this->request->setClientID(2619)
            ->setSessionToken('')
            ->setPassword('JpTwK4tNkm1VENv8V')
            ->setOrderCode('7184934552');

        $this->assertEquals([
                "ClientID" => 2619,
                "Password" => "JpTwK4tNkm1VENv8V",
                "SessionToken" => "",
                "GHNOrderCode" => "7184934552"
            ]
            , $this->request->getData());
    }
} 