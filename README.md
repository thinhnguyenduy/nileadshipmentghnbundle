# README #

A PHP library to interact with giaohangnhanh.vn 

### What is this repository for? ###

* This library is used for the upcoming hosted ecommerce solution on nilead.com.
* This library was written in such a way that it's possible to reuse in other PHP projects as well.

### How do I get set up? ###

* git or composer

### Contribution guidelines ###

* Writing tests
* Code review