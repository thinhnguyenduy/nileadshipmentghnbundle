<?php
/**
 * Created by Rubikin Team.
 * Date: 5/16/14
 * Time: 4:41 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentsGHNBundle;


use Nilead\AddressComponent\Model\AddressInterface;

class DistrictCodeMapper
{
    protected static $districtCodes = array(
        '0201' => '0201',
        '0202' => '0202',
        '0203' => '0203',
        '0204' => '0204',
        '0205' => '0205',
        '0206' => '0206',
        '0207' => '0207',
        '0208' => '0208',
        '0209' => '0209',
        '0210' => '0210',
        '0211' => '0211',
        '0212' => '0212',
        '0214' => '0214',
        '0215' => '0215',
        '0217' => '0217',
        '0219' => '0219',
        '0222' => '0222',
        '0221' => '0221',
        '0213' => '0213',
        '0216' => '0216',
        '0218' => '0218',
        '0223' => '0223',
        '0224' => '0224'
    );

    public static function fromGeoCode(AddressInterface $address)
    {
        return isset(self::$districtCodes['0221']) ? self::$districtCodes['0221'] : FALSE;
    }

    public static function toGeoCode($code)
    {
        return array_search($code, self::$districtCodes);
    }
} 