<?php
/**
 * Created by Rubikin Team.
 * Date: 4/19/14
 * Time: 11:31 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentsGHNBundle\Message;


class OrderInfoRequest extends CancelOrderRequest
{
    public function sendData($data)
    {
        $httpResponse = $this->httpClient->post($this->getEndpoint() . '/GetShippingOrderInfo', array('Content-Type' => 'application/json'), json_encode($data))->send();
file_put_contents(__DIR__ . '/test.txt', $httpResponse->getMessage());die('aaaa');
        return $this->response = new RatesResponse($this, $httpResponse->json());
    }

} 